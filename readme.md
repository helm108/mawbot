# MAWBot

Uses:

* https://github.com/yagop/node-telegram-bot-api

## How to develop locally.

* Install NodeJS: https://nodejs.org/
* Install Telegram: http://telegram.org/
* Open the terminal and navigate to the folder this file is in.
* Run `npm install`
* Open Telegram and talk to the Botfather.
* Create a new bot. Call it something like mawbotdev<whatever>bot.
* Copy the auth token that the Botfather gives you.
* Run MAWBot with the following command: `BOT_TOKEN=<thetokenthebotfathergaveyou> node server.js`

Commands sent to the bot you made should now be parsed by your copy of MAWBot.

## Command list for Botfather.

* which - Use this to make decisions for you. Returns one of the items passed in. For example: `/which Left, Right, Center` will return either Left, Right or Center.
* mawbot - Ask a question, get a Yes or a No

## Environment Variables Required
* BOT_TOKEN



