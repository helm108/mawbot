const {google} = require('googleapis');

const scopes = [
  'https://www.googleapis.com/auth/userinfo.email',
  'https://www.googleapis.com/auth/firebase.database'
];

const getAccessToken = function getAccessToken() {
  return new Promise((resolve, reject) => {
    const jwtClient = new google.auth.JWT(
      process.env.FIREBASE_EMAIL,
      null,
      process.env.FIREBASE_PRIVATE_KEY.replace(/\\n/g, '\n'),
      scopes
    );

    jwtClient.authorize((error, tokens) => {
      if (error) {
        reject(error);
      } else if (tokens.access_token === null) {
        reject("Provided service account does not have permission to generate access tokens");
      } else {
        resolve(tokens.access_token);
      }
    });
  });
};

module.exports = {
  getAccessToken,
};
