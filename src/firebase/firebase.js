const request = require('request');

const firebaseAuth = require('./firebase_auth');

let accessToken = null;
const getAccessToken = async function getAccessToken() {
  if (accessToken === null) {
    accessToken = await firebaseAuth.getAccessToken();
  }
  return accessToken;
};

const put = async function put(ref, data) {
  const url = `https://mawbot-1ac9a.firebaseio.com/${ref}.json?access_token=${await getAccessToken()}`;
  return new Promise((resolve, reject) => {
    request.put(url, {
      json: data,
    }, (err, res, body) => {
      if (err) {
        reject(err);
      }
      console.log('PUT');
      console.log('PUT: ref', ref);
      console.log('PUT: data', data);
      console.log('PUT: response', body);
      resolve(body);
    });
  });
};

const patch = async function put(ref, data) {
  const url = `https://mawbot-1ac9a.firebaseio.com/${ref}.json?access_token=${await getAccessToken()}`;
  return new Promise((resolve, reject) => {
    request.patch(url, {
      json: data,
    }, (err, res, body) => {
      if (err) {
        reject(err);
      }
      console.log('PATCH');
      console.log('PATCH: ref', ref);
      console.log('PATCH: data', data);
      console.log('PATCH: response', body);
      resolve(body);
    });
  });
};

const get = async function get(ref) {
  const url = `https://mawbot-1ac9a.firebaseio.com/${ref}.json?access_token=${await getAccessToken()}`;
  return new Promise((resolve, reject) => {
    request(url, { json: true }, (err, res, body) => {
      if (err) {
        reject(err);
      }
      console.log('GET');
      console.log('GET: ref', ref);
      console.log('GET: response', body);
      resolve(body);
    });
  });
};

module.exports = {
  put,
  patch,
  get,
};
