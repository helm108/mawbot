const TelegramBot = require('node-telegram-bot-api');

const modules = [
  require('./modules/yesno/index'),
  require('./modules/which/index'),
  require('./modules/gamble/index'),
];

const parseRequest = async function parseRequest(telegramRequest, bot) {
  for (let i = 0; i < modules.length; i++) {
    const module = modules[i];

    const regex = new RegExp(`\/${module.command} (.+)`);
    const matches = regex.exec(telegramRequest.message.text);

    if (matches !== null) {
      await module.parse(matches[1], telegramRequest, bot);
      exit();
    }
  }
};

const exit = function exit(m = 'blank exit')  {
  console.log(`Exit: ${m}`);
  return {
    statusCode: 200,
    body: m,
  };
};

exports.handler = async (event, context) => {
  if (event.body === '' || event.body === null) {
    console.log(`Exit: No body content.`);
    return {
      statusCode: 200,
      body: 'No body content.',
    };
  }

  let telegramRequest = null;

  try {
    console.log('Parse JSON');
    telegramRequest = JSON.parse(event.body);
  }
  catch (e) {
    console.log(`Exit: JSON parse failed.`);
    return {
      statusCode: 200,
      body: 'JSON parse failed.',
    };
  }

  if (telegramRequest['edited_message']) {
    telegramRequest.message = telegramRequest['edited_message'];
  }

  const bot = new TelegramBot(process.env.BOT_TOKEN, {polling: false});

  if (telegramRequest.message) {
    await parseRequest(telegramRequest, bot);
  }

  console.log('Exit: done!');
  return {
    statusCode: 200,
    body: 'done!',
  };
};

