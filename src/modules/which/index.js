const parser = require('./src/which');

module.exports = {
  command: 'which',
  parse(message, ctx, bot) {
    return parser.parse(message, ctx, bot);
  },
};
