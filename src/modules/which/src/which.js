// Split by commas.
// Else, split by spaces

const split = function(message) {
  const commas = message.indexOf(',');
  if (commas > -1) {
    const messageArr = message.split(',');

    return messageArr.reduce((arr, item) => {
      let trimmedItem = item.trim();
      if (trimmedItem !== '') {
        arr.push(item.trim());
      }
      return arr;
    }, []);
  }
  else {
    return message.split(' ');
  }
};

const parse = async function(message, ctx, bot) {
  const items = split(message);
  const index = Math.floor(Math.random() * items.length);
  await bot.sendMessage(ctx.message.chat.id, items[index]);
};

module.exports = {
  parse,
  split,
};
