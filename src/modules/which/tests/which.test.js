const which = require('../src/which');

test('Should split by commas', () => {
  const message = 'choice one, choice two, choice three';
  const expected = [
    'choice one',
    'choice two',
    'choice three'
  ];

  const actual = which.split(message);

  expect(actual).toEqual(expected);
});

test('Should split by spaces', () => {
  const message = 'one two three';
  const expected = [
    'one',
    'two',
    'three'
  ];

  const actual = which.split(message);

  expect(actual).toEqual(expected);
});

test('Should not return an empty choice', () => {
  const message = 'one ,,,,';
  const expected = [
    'one'
  ];

  const actual = which.split(message);

  expect(actual).toEqual(expected);
});
