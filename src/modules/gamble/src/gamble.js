const firebase = require('./firebase');
const dice = require('./dice');

async function playerWins(player) {
  player.balance = player.balance + player.amount;
  await firebase.setBalance(player);
  player.addOutcome(`${player.username}, you now have ${player.balance} yachts!`)
}

async function houseWins(player) {
  player.balance = player.balance - player.amount;
  await firebase.setBalance(player);
  player.addOutcome(`${player.username}, you now have ${player.balance} yachts.`)
}

async function determineOutcome(playerRoll, houseRoll, player, tiebreak = false) {
  if (!tiebreak) {
    player.addOutcome(`MAWBot rolled ${houseRoll}`);
  }
  player.addOutcome(`${player.username} rolled ${playerRoll}`);

  if (playerRoll < houseRoll) {
    player.addOutcome(`${player.username} loses!`);
    await houseWins(player);
  }
  else if (playerRoll > houseRoll) {
    player.addOutcome(`${player.username} wins!`);
    await playerWins(player);
  }
  else {
    if (tiebreak) {
      player.addOutcome('Second tie! MAWBot wins!');
      await houseWins(player);
    }
    else {
      player.addOutcome('Tie!');
      playerRoll = dice.roll1d6();
      await determineOutcome(playerRoll, houseRoll, player, true);
    }
  }
}

const validateBet = function validateBet(player) {
  const amount = player.amount;
  const balance = player.balance;

  let error = false;
  let message = null;

  if (amount === null) {
    message = 'You must supply an amount of yachts to bet.';
  }

  if (!Number.isInteger(amount)) {
    message = 'You must bet entire yachts.';
  }

  if (amount > balance) {
    message = `You don't have ${amount} yachts. You cannot bet more than ${balance} yachts.`;
  }

  if (amount < 0) {
    message = 'You cannot gamble negative yachts.';
  }

  if (amount === 0) {
    message = 'You must risk at least one yacht.';
  }

  if (message !== null) {
    error = true;
  }

  return {
    error,
    message,
  }
};

const bet = async function gamble(player) {
  const betValidation = validateBet(player);

  if (betValidation.error) {
    player.addOutcome(betValidation.message);
    return;
  }

  const houseRoll = dice.roll1d6();
  const playerRoll = dice.roll1d6();

  await determineOutcome(playerRoll, houseRoll, player);
};

module.exports = {
  validateBet,
  bet,
};
