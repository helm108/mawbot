function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1) + min);
}

module.exports = {
  roll1d6() {
    return getRandomInt(1, 6);
  }
};
