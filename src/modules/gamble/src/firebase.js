const firebase = require('../../../firebase/firebase');

const createPlayer = async function createPlayer(player) {
  return await firebase.put(`gamble/${player.id}`, {
    balance: player.balance,
    username: player.username,
    bankruptcies: 0,
  });
};

const getPlayerData = async function getPlayerData(player) {
  return await firebase.get(`gamble/${player.id}`);
};

const setBalance = async function setBalance(player) {
  return await firebase.patch(`gamble/${player.id}`, {
    balance: player.balance,
  });
};

const addBankruptcy = async function addBankruptcy(player) {
  return await firebase.patch(`gamble/${player.id}`, {
    bankruptcies: player.bankruptcies + 1,
    balance: player.balance,
  });
};

const getLeaderboard = async function getLeaderboard() {
  const leaderboardObj =  await firebase.get(`gamble`);

  const leaderboard = Object.keys(leaderboardObj).map((key) => {
    const username = leaderboardObj[key].username;
    const balance = leaderboardObj[key].balance;
    const bankruptcies =leaderboardObj[key].bankruptcies;
    return {
      username,
      balance,
      bankruptcies
    };
  });

  leaderboard.sort((a, b) => {
    return a.balance < b.balance;
  });

  let leaderboardString = '';
  for(let i = 0; i < leaderboard.length; i++) {
    let p = leaderboard[i];
    leaderboardString += `${i + 1}. ${p.username} with ${p.balance} yachts and ${p.bankruptcies} bankrupcties\n`;
  }

  return leaderboardString;
};

module.exports = {
  createPlayer,
  setBalance,
  addBankruptcy,
  getLeaderboard,
  getPlayerData,
};
