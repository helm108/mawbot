const firebase = require('./firebase');
const gamble = require('./gamble');

const DEFAULT_BALANCE = 100;

async function createPlayer(player) {
  await firebase.createPlayer(player);
  return `Welcome ${player.username}! You have ${player.balance} yachts.`;
}

async function outputOutcome(player, bot, chatId) {
  await bot.sendMessage(chatId, player.outcome.join('\n'), {
    reply_to_message_id: player.messageId,
  });
}

async function init(id, username, messageId, amount = null) {
  const player = {
    id,
    username,
    amount,
    balance: null,
    bankruptcies: null,
    outcome: [],
    messageId,
    addOutcome(m) {
      player.outcome.push(m);
    }
  };

  const playerData = await firebase.getPlayerData(player);

  if (playerData === null) {
    player.balance = DEFAULT_BALANCE;
    const result = await createPlayer(player);
    player.addOutcome(result);
  }
  else {
    player.balance = parseInt(playerData.balance);
    player.bankruptcies = playerData.bankruptcies;

    if (player.amount === 'balance') {
      const result = `${player.username}, your balance is ${player.balance}.`;
      player.addOutcome(result);
      if (player.balance === 0) {
        player.addOutcome('Declare bankruptcy to receive more yachts.');
      }
    }
    else if (player.amount === 'leaderboard') {
      const result = await firebase.getLeaderboard();
      player.addOutcome(result);
    }
    else if (player.amount === 'rules') {
      const rules = `Player and House each roll a die, higher number wins.
In ties, Player rolls again, must roll higher than the tie number (no more rerolls on ties).`;
      player.addOutcome(rules);
    }
    else if (player.balance === 0 && player.amount.includes('BANKRUPTCY')) {
      player.balance = DEFAULT_BALANCE;
      await firebase.addBankruptcy(player);
      const result = `${username} has declared bankruptcy! They have been donated ${DEFAULT_BALANCE} yachts.`;
      player.addOutcome(result);
    }
    else if (player.balance > 0 && player.amount.includes('BANKRUPTCY')) {
      const result = `You cannot declare bankruptcy if you have yachts!`;
      player.addOutcome(result);
    }
    else if (player.amount.toLowerCase().includes('bankruptcy')) {
      const result = `You have to DECLARE it!`;
      player.addOutcome(result);
    }
    else {
      player.amount = parseFloat(amount);
      await gamble.bet(player);
    }
  }

  return player;
}

const parse = async function parse(message, ctx, bot) {
  const id = ctx.message.from.id;
  const username = ctx.message.from.username;
  const amount = message;
  const player = await init(id, username, ctx.message.message_id, amount);
  await outputOutcome(player, bot, ctx.message.chat.id);
};

module.exports = {
  parse,
};
