const gamble = require('../src/gamble');

test('Amount must be a number', () => {
  const result = gamble.validateBet(null, 100);
  expect(result.error).toBe(true);
});

test('Cannot gamble more than balance', () => {
  const result = gamble.validateBet(200, 100);
  expect(result.error).toBe(true);
});

test('Cannot gamble a negative amount', () => {
  const result = gamble.validateBet(-200, 100);
  expect(result.error).toBe(true);
});

test('Cannot gamble nothing', () => {
  const result = gamble.validateBet(0, 100);
  expect(result.error).toBe(true);
});

test('Amount must be an integer', () => {
  const result = gamble.validateBet(10.5, 100);
  expect(result.error).toBe(true);
});

test('Can gamble entire balance', () => {
  const result = gamble.validateBet(100, 100);
  expect(result.error).toBe(false);
});

test('Can gamble a positive integer between 0 and balance', () => {
  const result = gamble.validateBet(10, 100);
  expect(result.error).toBe(false);
});
