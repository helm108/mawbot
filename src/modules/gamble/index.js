const parser = require('./src/index');

module.exports = {
  command: 'gamble',
  async parse(message, ctx, bot) {
    return await parser.parse(message, ctx, bot);
  },
};
