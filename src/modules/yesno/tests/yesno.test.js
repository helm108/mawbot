const yesno = require('../src/yesno');

const NO = 'No.';
const YES = 'Yes.';
const UHHH = 'Uhhhh.';
const UHHQ = 'Uhhhh?';

test('0 should return No.', () => {
  const expected = NO;
  const actual = yesno.reply(0);

  expect(actual).toBe(expected);
});

test('0.4749 should return No.', () => {
  const expected = NO;
  const actual = yesno.reply(0.4749);

  expect(actual).toBe(expected);
});

test('0.475 should return Yes.', () => {
  const expected = YES;
  const actual = yesno.reply(0.475);

  expect(actual).toBe(expected);
});

test('0.949 should return Yes.', () => {
  const expected = YES;
  const actual = yesno.reply(0.949);

  expect(actual).toBe(expected);
});

test('0.95 should return Uhhh.', () => {
  const expected = UHHH;
  const actual = yesno.reply(0.95);

  expect(actual).toBe(expected);
});

test('0.989 should return Uhhh.', () => {
  const expected = UHHH;
  const actual = yesno.reply(0.989);

  expect(actual).toBe(expected);
});

test('0.99 should return Uhhh?', () => {
  const expected = UHHQ;
  const actual = yesno.reply(0.99);

  expect(actual).toBe(expected);
});

test('1 should return Uhhh?', () => {
  const expected = UHHQ;
  const actual = yesno.reply(1);

  expect(actual).toBe(expected);
});
