const parser = require('./src/yesno');

module.exports = {
  command: 'mawbot',
  parse(message, ctx, bot) {
    return parser.parse(message, ctx, bot);
  },
};
