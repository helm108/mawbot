const seedRandom = require("seedrandom");

const reply = function(number) {
  if (number < 0.475) {
    return "No.";
  }
  else if (number < 0.95) {
    return "Yes.";
  }
  else if (number < 0.99) {
    return "Uhhhh.";
  }
  else {
    return "Uhhhh?";
  }
};

const parse = async function(message, ctx, bot) {
  const randomNumber = seedRandom.alea(message.toLowerCase())();
  await bot.sendMessage(ctx.message.chat.id, reply(randomNumber));
};

module.exports = {
  parse,
  reply,
};
